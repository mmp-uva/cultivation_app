Cultivation App
---------------

A shiny app for online monitoring and analysis of cultivation data
produced by

Installation
------------

Requires the following packages:

-   shiny
-   dplyr
-   gpplot2
-   gtable
-   grid
-   tidyr
-   ~~zoo~~ (No longer required)
-   ~~data.table~~ (No longer required)
-   dplyr
-   mmpr (&gt;= 0.5)
-   slider
-   DT

Usage
-----

### Shiny

Run the application on your computer in your browser with:

``` r
require(shiny)
runApp("./cultivation_app/")
```

When hosting, be sure to point shiny to the right folder. All shiny
logic is in the `./cultivation_app/` subfolder.

### Data

Data files (CSV or SQLITE) should be stored in the data folder in the
root of the application.
